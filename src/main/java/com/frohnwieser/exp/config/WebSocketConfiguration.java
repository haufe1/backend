package com.frohnwieser.exp.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

@Configuration
public class WebSocketConfiguration {
    @Autowired
    private WebSocketHandler webSocketHandler;

    @Bean
    @Qualifier("ExpWebSocketHandler")
    public HandlerMapping webSocketHandlerMapping() {
        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setOrder(Ordered.HIGHEST_PRECEDENCE);
        handlerMapping.setUrlMap(Collections.singletonMap("/ws/experts", webSocketHandler));
        return handlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter webSocketHandler() {
        return new WebSocketHandlerAdapter();
    }
}
