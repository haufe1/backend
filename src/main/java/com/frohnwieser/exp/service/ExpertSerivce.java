package com.frohnwieser.exp.service;

import com.frohnwieser.exp.channel.randomuser.RandomUserService;
import com.frohnwieser.exp.domain.ExpertDto;
import com.frohnwieser.exp.domain.Gender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;
import reactor.retry.Repeat;

@Service
public class ExpertSerivce {
    private final RandomUserService userService;
    // note: name chache also has application scope!
    private final NameCache nameCache;

    @Autowired
    public ExpertSerivce(RandomUserService userService, NameCache nameCache) {
        this.userService = userService;
        this.nameCache = nameCache;
    }
    
    public Mono<ExpertDto> loadExperts(@Nullable Gender gender) {
        return userService.getExpert(gender)
            // filter and repeat if name is already contained
            .filter(expert -> !nameCache.contains(expert.getFirstName()))
            .repeatWhenEmpty(Repeat.times(Long.MAX_VALUE))
            .doOnSuccess(expert -> nameCache.add(expert.getFirstName()));
    }
}
