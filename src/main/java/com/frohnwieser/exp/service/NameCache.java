package com.frohnwieser.exp.service;

import java.util.ArrayDeque;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@SuppressWarnings("serial") 
class NameCache extends ArrayDeque<String> {
    private static final int MAX_VALUES = 25;

    private NameCache() {
        super(MAX_VALUES);
    }

    @Override
    public boolean add(String value) {
        if (this.size() >= MAX_VALUES) {
            log.info("Removing duplicate value '" + value + "'.");
            removeLast();
        }
        return super.add(value);
    }
}
