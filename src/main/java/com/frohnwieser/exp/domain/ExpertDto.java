package com.frohnwieser.exp.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ExpertDto {
    private final String title;
    private final String firstName;
    private final String lastName;
    private final String gender;
    private final String photoUrl;
    private final AddressDto address;    
}
