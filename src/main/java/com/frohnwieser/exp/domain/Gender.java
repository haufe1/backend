package com.frohnwieser.exp.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Gender {
    FEMALE ("female"),
    MALE ("male");

    private final String gender;
}
