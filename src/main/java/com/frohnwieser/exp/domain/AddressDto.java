package com.frohnwieser.exp.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AddressDto {
    private final String streetName;
    private final String streetNumber;
    private final String city;
    private final String country;
    private final String postcode;
}
