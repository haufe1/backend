package com.frohnwieser.exp.channel.randomuser;

import com.fasterxml.jackson.databind.JsonNode;
import com.frohnwieser.exp.domain.ExpertDto;

public final class ExpertMapper {

    // see json format at https://randomuser.me/documentation
    public static ExpertDto map(JsonNode json) {
        return ExpertDto.builder()
            .title(json.at("/name/title").asText())
            .firstName(json.at("/name/first").asText())
            .lastName(json.at("/name/last").asText())
            .gender(json.at("/gender").asText())
            .photoUrl(json.at("/picture/medium").asText())
            .address(AddressMapper.map(json.at("/location")))
            .build(); 
    }    
}
