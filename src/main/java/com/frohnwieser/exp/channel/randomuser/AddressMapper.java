package com.frohnwieser.exp.channel.randomuser;

import com.fasterxml.jackson.databind.JsonNode;
import com.frohnwieser.exp.domain.AddressDto;

public final class AddressMapper {
    
    // see json format at https://randomuser.me/documentation
    public static AddressDto map(JsonNode json) {
        return AddressDto.builder()
            .streetName(json.at("/street/name").asText())
            .streetNumber(json.at("/street/number").asText())
            .city(json.at("/city").asText())
            .country(json.at("/country").asText())
            .postcode(json.at("/postcode").asText())
            .build(); 
    }    
}
