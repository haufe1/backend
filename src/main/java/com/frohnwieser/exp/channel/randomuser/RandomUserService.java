package com.frohnwieser.exp.channel.randomuser;

import java.time.Duration;

import com.fasterxml.jackson.databind.JsonNode;
import com.frohnwieser.exp.domain.ExpertDto;
import com.frohnwieser.exp.domain.Gender;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

@Slf4j
@Service
public class RandomUserService {
    private final String apiUrl;
    private final WebClient client;

    public RandomUserService(@Value("${randomuser.api}") String apiUrl) {
        this.apiUrl = apiUrl;
        client = WebClient.create();
    }    

    public Mono<ExpertDto> getExpert(@Nullable Gender gender) {
        log.info("Got gender " + gender);

        return client.get()
            .uri(apiUrl + "/?inc=name,gender,location,picture" + (gender != null ? "&gender=" + gender.getGender() : ""))
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .map(json -> json.at("/results").get(0))
            .map(ExpertMapper::map)
            .doOnNext(expert -> log.info("Received expert: " + expert))
            .retryWhen(Retry.backoff(3, Duration.ofMillis(250)));
    }
}
