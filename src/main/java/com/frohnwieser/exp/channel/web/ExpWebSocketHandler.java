package com.frohnwieser.exp.channel.web;

import java.time.Duration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.frohnwieser.exp.service.ExpertSerivce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;

import reactor.core.publisher.Mono;
import reactor.retry.Repeat;

@Component("ExpWebSocketHandler")
// @Scope(value = "websocket", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ExpWebSocketHandler implements WebSocketHandler {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final ExpertSerivce expertSerivce;

    @Value("${randomuser.intervalMaxMs:2000}")
    private int intervalMaxMs;
    @Value("${randomuser.intervalMinMs:2000}")
    private int intervalMinMs;

    @Autowired 
    public ExpWebSocketHandler(ExpertSerivce expertSerivce) {
        this.expertSerivce = expertSerivce;
    }
    
    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        final GenderBean gender = new GenderBean();
        return webSocketSession
            // defer to read new values for gender
            .send(Mono.defer(() -> expertSerivce.loadExperts(gender.getGender()))
                    // repeat continually in random interval
                    .repeatWhen(Repeat.times(Long.MAX_VALUE)
                    .randomBackoff(Duration.ofMillis(intervalMinMs), Duration.ofMillis(intervalMaxMs)))
                .map(ExpWebSocketHandler::toJsonString)
                .map(webSocketSession::textMessage))
            .and(webSocketSession.receive()
                .map(WebSocketMessage::getPayloadAsText)
                // somehow the message is wrapped in an addtional ""
                .doOnNext(message -> gender.from(message.replace("\"", ""))));
    }

    private static String toJsonString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
        }
        return "{}";
    }
}
