package com.frohnwieser.exp.channel.web;

import com.frohnwieser.exp.domain.Gender;

import lombok.Data;

@Data
public class GenderBean {
    private Gender gender;    

    public void from(String value) {
        try {
            gender = Gender.valueOf(value);
        } catch (Exception e) {
            gender = null;
        }
    }
}
