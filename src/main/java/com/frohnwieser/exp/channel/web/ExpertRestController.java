package com.frohnwieser.exp.channel.web;

import com.frohnwieser.exp.channel.randomuser.RandomUserService;
import com.frohnwieser.exp.domain.ExpertDto;
import com.frohnwieser.exp.domain.Gender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import reactor.core.publisher.Mono;

@RestController()
@RequestMapping(value = "/api/experts", produces = "application/json")
@Tag(name = "Expert API", description = "Expert API")
public class ExpertRestController {
    private final RandomUserService userService;

    public ExpertRestController(@Autowired RandomUserService userService) {
        this.userService = userService;
    }
    
    @GetMapping("")
    @Operation(description = "Gets a an expert")
    public Mono<ExpertDto> getExpert(@ApiParam(name = "gender", type = "Gender", example = "female", required = false)
        @RequestParam(required = false) Gender gender) {
        return userService.getExpert(gender);
    }
}
