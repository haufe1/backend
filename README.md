# EXP Backend

## API
The API definition can be found in the [Swagger documentation](http://localhost:8080/swagger-ui/)

## Authentication
For Basic Auth use user: `user` with password: `pass`